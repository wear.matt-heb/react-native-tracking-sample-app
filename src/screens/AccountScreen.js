import React, {useContext} from 'react';
import {StyleSheet, Text} from 'react-native';
import { Button } from 'react-native-elements';
import Spacer from '../components/Spacer';
import { Context as AuthContext } from '../context/AuthContext';
import { SafeAreaView } from 'react-navigation';


const AccountScreen = () => {
    const { signout } = useContext(AuthContext);

    return ( 
        <SafeAreaView style={styles.container}>
            <Spacer>
                <Text>Account Screen</Text>
            </Spacer>
            <Spacer>
                <Button 
                    title="Sign Out"
                    onPress={signout}
                />
            </Spacer>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
  });

export default AccountScreen;